tup.creategitignore()
rule = {}
each = {}
do
  files = {}
  files.relatives = {}
  files.inputs = {}

  function outfile(relative)
    local input = string.format('out/dest/%s', relative)
    table.insert(files.relatives, relative)
    table.insert(files.inputs, input)
    return input
  end

  function rule.archive()
    local tgz = 'out/website.tar.gz'
    local cmd = '^ TAR %%o^ cd out/dest; tar -czf ../../%%o %s'
    local argument = tostring(files.relatives)

    tup.rule(files.inputs, cmd:format(argument), tgz)
  end
end

function rule.copy(input, path)
  local path = path or input
  local command
    = string.format('^ MINIFY %s^ minify --mime text/html <%%f>%%o', path)
  tup.rule(input, command, outfile(path))
end

function each.glob(fn, globs)
  if type(globs) == "string" then
    return each.glob({globs}, fn)
  end
  for _, glob in ipairs(globs) do
    for _, file in ipairs(tup.glob(glob)) do
      fn(file)
    end
  end
end

each.glob(rule.copy, {"index.html", "articles/*.html"})
tup.rule( tup.glob('articles/*.html')
        , './aux/mk-articles-list %f >%o'
        , 'out/articles.html')
rule.copy('out/articles.html', 'articles.html')
rule.archive()

